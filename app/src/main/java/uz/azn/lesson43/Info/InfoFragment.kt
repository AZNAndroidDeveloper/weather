package uz.azn.lesson43.Info

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import org.json.JSONException
import uz.azn.lesson43.R
import uz.azn.lesson43.databinding.FragmentInfoBinding
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt

class InfoFragment : Fragment(R.layout.fragment_info) {
    private lateinit var binding: FragmentInfoBinding
    val api_key = "f70531640cd6706f7691a45f80c0225b"
    private var requestQueue: RequestQueue? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentInfoBinding.bind(view)
        requestQueue = Volley.newRequestQueue(requireContext())
        val arguments = arguments
        val name = arguments?.getString("name")
        Log.d("name", "onViewCreated: $name")
        binding.tvCountryName.text = name
        getWeather(name!!)
    }

    @SuppressLint("SimpleDateFormat")
    private fun getWeather(country: String) {
        val URL = "https://api.openweathermap.org/data/2.5/weather?q=$country&appid=$api_key"
        val jsonObjectRequest =
            JsonObjectRequest(Request.Method.GET, URL, null, Response.Listener { response ->
                try {

                    val mainObject = response!!.getJSONObject("main")
                    val weatherObject = response.getJSONArray("weather")
                    val jsonObject = weatherObject.getJSONObject(0)
                    val resTemp = mainObject.getDouble("temp").toString()
                    val responseDescription = jsonObject.getString("description")
                    val resicon = jsonObject.getString("icon")
                    val resname = response.getString("name")
                    val iconUrl = "https://openweathermap.org/img/w/$resicon.png"


                    val tempInt = resTemp.toDouble()
                    var celsie = tempInt - 273.15
                    celsie = celsie.roundToInt().toDouble()

                    val calendar = Calendar.getInstance()
                    val simpleDateFormat = SimpleDateFormat("dd-MM-yyyy")
                    val formatDate = simpleDateFormat.format(calendar.time)
                    Log.d(
                        "TAG",
                        "getWeather: $resTemp $resname,$responseDescription,$celsie $formatDate"
                    )
                    with(binding) {
                        progress.visibility = View.INVISIBLE
                        tvDate.text = formatDate
                        tvDesc.text = responseDescription
                        tvTemp.text = celsie.toString()
                        Picasso.get().load(iconUrl).into(ivIcon)
                    }
//
                } catch (ex: JSONException) {
                    ex.printStackTrace()
                    with(binding) {
                        progress.visibility = View.INVISIBLE
                    }
                }
            }, Response.ErrorListener { error ->
                with(binding) {
                    progress.visibility = View.INVISIBLE
                }
                error.printStackTrace()
            })

        val queue = Volley.newRequestQueue(requireContext())
        queue.add(jsonObjectRequest)
    }

}