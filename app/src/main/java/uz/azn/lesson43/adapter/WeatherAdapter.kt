package uz.azn.lesson43.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.recyclerview.widget.RecyclerView
import uz.azn.lesson43.countryModel.Countries
import uz.azn.lesson43.databinding.RowItemBinding as RowBinding

class WeatherAdapter (private val onItemClickListener:(Countries)->Unit):RecyclerView.Adapter<WeatherAdapter.WeatherViewHolder>(){
    val countriesList = mutableListOf<Countries>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherViewHolder  = WeatherViewHolder(RowBinding.inflate(
        LayoutInflater.from(parent.context),parent,false))

    override fun getItemCount(): Int  = countriesList.size

    override fun onBindViewHolder(holder: WeatherViewHolder, position: Int) {
  holder.onBind(countriesList[position])
    }
    inner class WeatherViewHolder(val binding: RowBinding):RecyclerView.ViewHolder(binding.root){
fun onBind(element:Countries){
    with(binding){
        image.setImageResource(element.image)
        tvCountryName.text = element.countryName
        linear.setOnClickListener {
            onItemClickListener.invoke(element)
        }
    }
}

    }
fun setCountry(element:MutableList<Countries>){
    countriesList.apply { clear(); addAll(element) }
}
}