package uz.azn.lesson43.intro

import android.annotation.SuppressLint
import android.app.DownloadManager
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import org.json.JSONException
import uz.azn.lesson43.Info.InfoFragment
import uz.azn.lesson43.R
import uz.azn.lesson43.adapter.WeatherAdapter
import uz.azn.lesson43.countryModel.Countries
import uz.azn.lesson43.databinding.FragmentIntroBinding
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt

//const val URL = "https://jsonplaceholder.typicode.com/comments"
class IntroFragment : Fragment(R.layout.fragment_intro) {
    private lateinit var binding: FragmentIntroBinding

    private val weatherAdapter by lazy {
        WeatherAdapter(onItemClickListener = {
            Toast.makeText(requireContext(), "${it.countryName}", Toast.LENGTH_SHORT).show()
            val bundle = Bundle()
            bundle.putString("name", it.countryName)
            val infoFragment = InfoFragment()
            infoFragment.arguments = bundle
            fragmentManager!!.beginTransaction().disallowAddToBackStack().replace(R.id.frame_layout, infoFragment).commit()

        })

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentIntroBinding.bind(view)
        with(binding) {
            recycler.apply {
                layoutManager = LinearLayoutManager(requireContext())
                adapter = weatherAdapter
            }
        }
        weatherAdapter.setCountry(getCountries())
    }


    fun getCountries(): MutableList<Countries> {
        return mutableListOf(
            Countries("Toshkent", R.drawable.toshkent),
            Countries("Namangan", R.drawable.namangan),
            Countries("Andijon", R.drawable.andijon),
            Countries("Fargona", R.drawable.fargona),
            Countries("Sirdaryo", R.drawable.sirdaryo),
            Countries("Navoiy", R.drawable.navoiy),
            Countries("Xorazm", R.drawable.xorazmjpg),
            Countries("Samarqand", R.drawable.samarqand),
            Countries("Qashqadaryo", R.drawable.qashqadaryo),
            Countries("Surxondaryo", R.drawable.surxondaryo)
        )
    }

//    private fun jsonParse(){
//        val request = JsonArrayRequest(Request.Method.GET, URL, null, Response.Listener {
//            response ->
//            try {
//                for (i in 0 until response.length()){
//                    val user = response.getJSONObject(i)
//                    val title = user.getString("title")
//                    val body = user.getString("body")
//                }
//
//            }catch (ex:JSONException){
//                ex.printStackTrace()
//            }
//        },Response.ErrorListener {
//            error -> error.printStackTrace()
//        })
//        requestQueue?.add(request)
//    }


}